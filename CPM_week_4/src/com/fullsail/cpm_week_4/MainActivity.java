/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.Parse;

public class MainActivity extends Activity implements PropertyChangeListener {
	
	public static Context _context;
	Connectivity _connection;
	Boolean ascending;
	SQLiteDatabase db;
	ArrayList<Celebrity> celebrities = new ArrayList<Celebrity>();
	String whereField;
	ListView _list;
	ListViewAdapter _adapter;
	Connectivity connection;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		 
		_context = this;
		CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
		db = celebrityTable.getReadableDatabase();
		// Pass results to ListViewAdapter Class
        _adapter = new ListViewAdapter(_context, celebrities);

        // Locate the ListView
        _list = (ListView) findViewById(R.id.listView1);

        // Binds the Adapter to the ListView
        if (_list != null) {
                _list.setAdapter(_adapter);
        }
        
		SQLManager.addChangeListener(this);
		
		Parse.initialize(this, "My0Q7LSHhzLkLII0vC1QaL9GLeVS9bbZR8fsEB7a", "n73cCNz0X81pTVLijalUsncZX7Q1xMThNMGhDnMK"); 
		
		connection = new Connectivity();
		ascending = true;
		
		// Download celebrities from DB
		Sync.sharedInstance().downloadCelebrities();
		
		// Setup the timer
//		Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                	Sync.sharedInstance().downloadCelebrities();
//                }
//        }, 0, 10000);
		
		// Setup button listeners
		EditText editTextAge = (EditText)findViewById(R.id.filterAgeText);
		editTextAge.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){
				Log.i("TextWatcher", s.toString());
				whereField = CelebrityTable.COLUMN_AGE;
				queryWithText(s.toString());
			}
		});
		
		EditText editTextId = (EditText)findViewById(R.id.filterIdText);
		editTextId.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){
				Log.i("TextWatcher", s.toString());
				whereField = CelebrityTable.COLUMN_CELEB_ID;
				queryWithText(s.toString());
			}
		});
		
		Button lastNameButton = (Button) findViewById(R.id.lastNameButton);
		lastNameButton.setOnClickListener(new OnClickListener(){
	        @Override
	        public void onClick(View view) {
	        	whereField = CelebrityTable.COLUMN_LASTNAME;
	        	sortFields();
	        }
	    });
		
		Button heightButton = (Button) findViewById(R.id.heightButton);
		heightButton.setOnClickListener(new OnClickListener(){
	        @Override
	        public void onClick(View view) {
	        	whereField = CelebrityTable.COLUMN_HEIGHT_IN_METERS;
	        	sortFields();
	        }
	    });
		
		Button resetButton = (Button) findViewById(R.id.resetButton);
		resetButton.setOnClickListener(new OnClickListener(){
	        @Override
	        public void onClick(View view) {
	        	queryAllRecords();
	        }
	    });
		
		Button addCelebButton = (Button) findViewById(R.id.addCelebButton);
		addCelebButton.setOnClickListener(new OnClickListener(){
	        @Override
	        public void onClick(View view) {
	        	// Go to add activity
	        	Intent intent = new Intent(MainActivity.this, CreateCeleb.class);
			    startActivity(intent);
	        }
	    });
		
		// Capture clicks on ListView items
		_list.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                	 // Go to add view and populate with celeb values.
                	Celebrity celebrity = celebrities.get(position);
                	if (celebrity != null) {
                		Intent intent = new Intent(MainActivity.this, CreateCeleb.class);
        	        	intent.putExtra(CelebrityTable.COLUMN_FIRSTNAME, celebrity.getFirstname());
        	        	intent.putExtra(CelebrityTable.COLUMN_LASTNAME, celebrity.getLastname());
        	        	intent.putExtra(CelebrityTable.COLUMN_AGE, String.valueOf(celebrity.getAge()));
        	        	intent.putExtra(CelebrityTable.COLUMN_BIRTHDATE, celebrity.getBirthdate());
        	        	intent.putExtra(CelebrityTable.COLUMN_BIRTH_PLACE, celebrity.getBirth_place());
        	        	intent.putExtra(CelebrityTable.COLUMN_HEIGHT_IN_METERS, String.valueOf(celebrity.getHeight_in_meters()));
        	        	intent.putExtra(CelebrityTable.COLUMN_CELEB_ID, celebrity.getCeleb_id());
        			    startActivity(intent);
                	}
                }
        });

		_list.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				Celebrity celebrity = celebrities.get(position);
				SQLManager.sharedInstance().deleteCelebrityById(celebrity.getCeleb_id());
				return false;
			}
		});
	}
	
	public void queryAllRecords() {
		String query = "SELECT * " + "FROM " + CelebrityTable.TABLE_NAME;
		Cursor cursor = db.rawQuery(query, null);
		queryWithCursor(cursor);
	}
	
	public void queryWithText(String s) {
		celebrities = new ArrayList<Celebrity>();
		if (s.length() == 0) {
			refreshListView();
			return;
		}

		String query = "SELECT * " 
				+ "FROM " + CelebrityTable.TABLE_NAME 
				+ " WHERE " + whereField + " LIKE '%"+s+"%';";
		Cursor cursor = db.rawQuery(query, new String[] {});
		queryWithCursor(cursor);
	}

	public void sortFields() {
		ascending = !ascending;
		String ASCString = ascending ? "ASC" : "DESC";
		celebrities = new ArrayList<Celebrity>();
		String query = "SELECT * " 
				+ "FROM " + CelebrityTable.TABLE_NAME 
				+ " ORDER BY " + whereField + " " + ASCString;
		Cursor cursor = db.rawQuery(query, null);
		queryWithCursor(cursor);
	}
	
	private void queryWithCursor(Cursor cursor) {
		Celebrity celebrity = null;
		if (cursor.moveToFirst()) {
			do {
				celebrity = SQLManager.sharedInstance().cursorToCelebrity(cursor);
				// Add celebrity to array
				celebrities.add(celebrity);
			} while (cursor.moveToNext());
		}
		cursor.close();
		refreshListView();
	}

	public void refreshListView() {
		// Pass results to ListViewAdapter Class
		_adapter = new ListViewAdapter(this, celebrities);
		_list.setAdapter(_adapter);
		((ListViewAdapter)_list.getAdapter()).notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// Listen for notification
		if (event.getPropertyName().equals("didSave") || event.getPropertyName().equals("didDownload")) {
			queryAllRecords();
		}
	}

}
