/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CelebrityTable extends SQLiteOpenHelper {
	public CelebrityTable(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

    private static final String DATABASE_NAME = "celebritytable.db";
    private static final int DATABASE_VERSION = 1;
    
    // Database table
	public static final String TABLE_NAME = "celebrity";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRSTNAME = "firstname";
    public static final String COLUMN_LASTNAME = "lastname";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_BIRTHDATE = "date";
    public static final String COLUMN_BIRTH_PLACE = "birth_place";
    public static final String COLUMN_HEIGHT_IN_METERS = "height_in_meters";
    public static final String COLUMN_CELEB_ID = "celeb_id";
    
    
    // Database creation sql statement
    private static final String TABLE_CREATE = "create table "
        + TABLE_NAME + "(" 
        + COLUMN_ID + " integer primary key autoincrement, " 
        + COLUMN_FIRSTNAME + " text not null,"
        + COLUMN_LASTNAME + " text not null,"
        + COLUMN_AGE + " int not null,"
        + COLUMN_BIRTHDATE + " text not null,"
        + COLUMN_BIRTH_PLACE + " text not null,"
        + COLUMN_HEIGHT_IN_METERS + " text not null,"
    	+ COLUMN_CELEB_ID + " text not null);";

 
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }
    
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.w(CelebrityTable.class.getName(), "Upgrading database from version "
		        + oldVersion + " to " + newVersion
		        + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
	}
}
