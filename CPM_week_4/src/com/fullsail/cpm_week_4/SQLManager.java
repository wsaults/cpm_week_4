/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.parse.ParseObject;

public class SQLManager {

	private static SQLManager instance = null;
	
	protected SQLManager() {
		// Exists only to defeat instantiation.
	}
	public static SQLManager sharedInstance() {
		if(instance == null) {
			instance = new SQLManager();
		}
		return instance;
	}
	
	private static List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>(); 
	public static void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}
	
	public void createCelebrityWithValues(String first_name, String last_name, int age, String birthdate, String birth_place, float height_in_meters, String celeb_id) {
		if (fetchCelebrityById(celeb_id) == null) {
			// If the celebrity does not exist locally then add it to the local db.
			CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
			SQLiteDatabase db = celebrityTable.getReadableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(CelebrityTable.COLUMN_FIRSTNAME, first_name);
			cv.put(CelebrityTable.COLUMN_LASTNAME, last_name);
			cv.put(CelebrityTable.COLUMN_AGE, age);
			cv.put(CelebrityTable.COLUMN_BIRTHDATE, birthdate);
			cv.put(CelebrityTable.COLUMN_BIRTH_PLACE, birth_place);
			cv.put(CelebrityTable.COLUMN_HEIGHT_IN_METERS, height_in_meters);
			cv.put(CelebrityTable.COLUMN_CELEB_ID, celeb_id);
			db.insert(CelebrityTable.TABLE_NAME, null, cv);
			db.close();
			
			Log.d("createCelebrityWithValues", "Celebrity created with id: " + celeb_id);
			
			Celebrity celeb = new Celebrity();
			celeb.setFirstname(first_name);
			celeb.setLastname(last_name);
			celeb.setAge(age);
			celeb.setBirthdate(birthdate);
			celeb.setBirth_place(birth_place);
			celeb.setHeight_in_meters(height_in_meters);
			celeb.setCeleb_id(celeb_id);
			
	        Sync.sharedInstance().createCelebrity(celeb);
	        
	        for (PropertyChangeListener name : listener) {
				name.propertyChange(new PropertyChangeEvent(this, "didSave", null, null));
			}
		} else {
			Toast.makeText(MainActivity._context, "That celebrity already exists in the database.", Toast.LENGTH_LONG).show();
		}
	}
	
	public void updateCelebrityWithValues(String first_name, String last_name, int age, String birthdate, String birth_place, float height_in_meters, String celeb_id) {
		Celebrity celeb = fetchCelebrityById(celeb_id);
		if (celeb != null) {
			// If the celebrity does exist locally then update it.
			CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
			SQLiteDatabase db = celebrityTable.getReadableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(CelebrityTable.COLUMN_FIRSTNAME, first_name);
			cv.put(CelebrityTable.COLUMN_LASTNAME, last_name);
			cv.put(CelebrityTable.COLUMN_AGE, age);
			cv.put(CelebrityTable.COLUMN_BIRTHDATE, birthdate);
			cv.put(CelebrityTable.COLUMN_BIRTH_PLACE, birth_place);
			cv.put(CelebrityTable.COLUMN_HEIGHT_IN_METERS, height_in_meters);
			cv.put(CelebrityTable.COLUMN_CELEB_ID, celeb_id);
			db.update(CelebrityTable.TABLE_NAME, cv, CelebrityTable.COLUMN_CELEB_ID + " = ?", new String[] {celeb_id});
			db.close();
			
			celeb.setFirstname(first_name);
			celeb.setLastname(last_name);
			celeb.setAge(age);
			celeb.setBirthdate(birthdate);
			celeb.setBirth_place(birth_place);
			celeb.setHeight_in_meters(height_in_meters);
			
			Sync.sharedInstance().updateCelebrity(celeb);
			
			// Notify of didSave
			for (PropertyChangeListener name : listener) {
				name.propertyChange(new PropertyChangeEvent(this, "didSave", null, null));
			}
			
		} else {
			// If the celebrity does not exist locally then add it to the local db.
			createCelebrityWithValues(first_name, last_name, age, birthdate, birth_place, height_in_meters, celeb_id);
		}
	}
		
	public void updateLocalCelebrities(List<ParseObject> celebrities) {
		CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
		SQLiteDatabase db = celebrityTable.getReadableDatabase();
		
		for (ParseObject celebrity : celebrities) {
			// Get the date in "dd-MMM-yy" format
			ContentValues cv = contentValuesForParseObject(celebrity);
			Celebrity celeb = fetchCelebrityById(celebrity.getString("celeb_id"));
			if (celeb != null) {
				// Update new Celebrity in sqlite db
				db.update(CelebrityTable.TABLE_NAME, cv, CelebrityTable.COLUMN_CELEB_ID + " = ?", new String[] {celebrity.getString("celeb_id")});
			} else {
				// Create new Celebrity in sqlite db
		        db.insert(CelebrityTable.TABLE_NAME, null, cv);
			}
		}
		db.close();
		
		// Notify of didSave
		for (PropertyChangeListener name : listener) {
			name.propertyChange(new PropertyChangeEvent(this, "didSave", null, null));
		}
	}
	
	private ContentValues contentValuesForParseObject(ParseObject obj) {
		ContentValues cv = new ContentValues();
		cv.put(CelebrityTable.COLUMN_FIRSTNAME, obj.getString("firstname"));
		cv.put(CelebrityTable.COLUMN_LASTNAME, obj.getString("lastname"));
		cv.put(CelebrityTable.COLUMN_AGE, obj.getInt("age"));
		cv.put(CelebrityTable.COLUMN_BIRTHDATE, obj.getString("birthdate"));
		cv.put(CelebrityTable.COLUMN_BIRTH_PLACE, obj.getString("birth_place"));
		cv.put(CelebrityTable.COLUMN_HEIGHT_IN_METERS, (float)obj.getDouble("height_in_meters"));
		cv.put(CelebrityTable.COLUMN_CELEB_ID, obj.getString("celeb_id"));
		return cv;
	}

	public void deleteCelebrityById(String celeb_id) {
		Celebrity celeb = fetchCelebrityById(celeb_id);
		if (celeb != null) {
			// Delete the celeb on the remote db.
			Sync.sharedInstance().deleteCelebrity(celeb);
			// Delete the celeb locally.
			CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
			SQLiteDatabase db = celebrityTable.getReadableDatabase();
			db.delete(CelebrityTable.TABLE_NAME, CelebrityTable.COLUMN_CELEB_ID + " = ?", new String[] {celeb_id});
			db.close();
			
			// Notify of didSave
			for (PropertyChangeListener name : listener) {
				name.propertyChange(new PropertyChangeEvent(this, "didSave", null, null));
			}
		} else {
			Toast.makeText(MainActivity._context, "That celebrity does not exist in the database.", Toast.LENGTH_LONG).show();
		}
	}

	public Celebrity fetchCelebrityById(String celeb_id) {
		// Get Celebrity from sqlite
		CelebrityTable celebrityTable = new CelebrityTable(MainActivity._context);
		SQLiteDatabase db = celebrityTable.getReadableDatabase();
		
		String query = "select * from " + CelebrityTable.TABLE_NAME + " where " + CelebrityTable.COLUMN_CELEB_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[] {celeb_id});
        Celebrity celebrity = null;
        if (cursor.moveToFirst()) {
        	celebrity = cursorToCelebrity(cursor);
        }
	    cursor.close();
		return celebrity;
	}
	
	public Celebrity cursorToCelebrity(Cursor cursor) {
		Celebrity celebrity = new Celebrity(); // TODO: double check the cursor index values
		celebrity.setFirstname(cursor.getString(1));
		celebrity.setLastname(cursor.getString(2));
		celebrity.setAge(cursor.getInt(3));
		celebrity.setBirthdate(cursor.getString(4));
		celebrity.setBirth_place(cursor.getString(5));
		celebrity.setHeight_in_meters(cursor.getFloat(6));
		celebrity.setCeleb_id(cursor.getString(7));

		return celebrity;
  }
}
