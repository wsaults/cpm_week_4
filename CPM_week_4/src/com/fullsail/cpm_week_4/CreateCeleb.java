/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class CreateCeleb extends Activity implements PropertyChangeListener{
	Dialog picker;
	Button set;
	DatePicker datep;
	Integer month,day,year;
	Button selectDateButton;
	EditText firstNameText;
	EditText lastNameText;
	EditText ageText;
	EditText heightText;
	EditText idText;
	EditText birthPlaceText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_celeb);
		// Show the Up button in the action bar.
		setupActionBar();
		
		SQLManager.addChangeListener(this);
		
		firstNameText = (EditText) findViewById(R.id.firstNameEditText);
		lastNameText = (EditText) findViewById(R.id.lastNameEditText);
		ageText = (EditText) findViewById(R.id.ageEditText);
		heightText = (EditText) findViewById(R.id.heightEditText);
		idText = (EditText) findViewById(R.id.idEditText);
		birthPlaceText = (EditText) findViewById(R.id.birthPlaceEditText);
		selectDateButton = (Button)findViewById(R.id.selectDate);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			firstNameText.setText(extras.getString(CelebrityTable.COLUMN_FIRSTNAME));
			lastNameText.setText(extras.getString(CelebrityTable.COLUMN_LASTNAME));
			ageText.setText(extras.getString(CelebrityTable.COLUMN_AGE));
			heightText.setText(extras.getString(CelebrityTable.COLUMN_HEIGHT_IN_METERS));
			idText.setText(extras.getString(CelebrityTable.COLUMN_CELEB_ID));
			birthPlaceText.setText(extras.getString(CelebrityTable.COLUMN_BIRTH_PLACE));
			selectDateButton.setText(extras.getString(CelebrityTable.COLUMN_BIRTHDATE));
			// TODO: update date picker.
		} else {
			// TODO: set date to today
//			selectDateButton.setText(extras.getString(CelebrityTable.COLUMN_BIRTHDATE));
		}
		
		selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                picker = new Dialog(CreateCeleb.this);
                picker.setContentView(R.layout.picker_frag);
                picker.setTitle("Select Date and Time");
 
                datep = (DatePicker)picker.findViewById(R.id.datePicker);
                set = (Button)picker.findViewById(R.id.btnSet);
                set.setOnClickListener(new View.OnClickListener() {
 
                    @Override
                    public void onClick(View view) {
                        // TODO Auto-generated method stub
                        month = datep.getMonth();
                        day = datep.getDayOfMonth();
                        year = datep.getYear();
 
                        selectDateButton.setText(day + "-" + month + "-" + year);
                        picker.dismiss();
                    }
                });
                picker.show();
            }
        }); 
		
		Button submitButton = (Button) findViewById(R.id.submit);
		submitButton.setOnClickListener(new OnClickListener(){
	        @Override
	        public void onClick(View view) {
	            if (firstNameText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing first name. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (lastNameText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing last name. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (ageText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing age. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (heightText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing height. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (idText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing ID. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (birthPlaceText.getText().length() == 0) {
	            	Toast.makeText(MainActivity._context, "Missing birth place. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            if (selectDateButton.getText().equals("Birth date")) {
	            	Toast.makeText(MainActivity._context, "Missing birth place. This field is required.", Toast.LENGTH_LONG).show();
	                return;
	            }
	            
	            int age = Integer.parseInt(ageText.getText().toString());
	            float height = Float.parseFloat(heightText.getText().toString());

	            SQLManager.sharedInstance().updateCelebrityWithValues(firstNameText.getText().toString(), lastNameText.getText().toString(), age, selectDateButton.getText().toString(), birthPlaceText.getText().toString(), height, idText.getText().toString());
	        }
	    });
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_celeb, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// Listen for notification
		if (event.getPropertyName().equals("didSave")) {
			finish();
		}
	}

}
