/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter {
	 
    Context context;
    ArrayList<Celebrity> celebrities;
    LayoutInflater inflater;
 
    public ListViewAdapter(Context context, ArrayList<Celebrity> vehicles) {
        this.context = context;
        this.celebrities = vehicles;
    }
 
    public int getCount() {
        return celebrities.size();
    }
 
    public Object getItem(int position) {
        return null;
    }
 
    public long getItemId(int position) {
        return 0;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	// "First Last | Age | B-Day | Location | Height | ID"
    	Celebrity celebrity = celebrities.get(position);
    	String message = celebrity.getFirstname() + " "
    				   + celebrity.getLastname() + ", " 
    				   + celebrity.getAge() + ", " 
    				   + celebrity.getBirthdate() + ", " 
    				   + celebrity.getBirth_place() + ", "
    				   + celebrity.getHeight_in_meters() + ", "
    				   + celebrity.getCeleb_id();

        // Declare Variables
        TextView messageTextView;
 
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
        View itemView = inflater.inflate(R.layout.listview_item, parent, false);
 
        // Locate the TextViews in listview_item.xml
        messageTextView = (TextView) itemView.findViewById(R.id.messageLabel);
        messageTextView.setText(message);
 
        return itemView;
    }
}
