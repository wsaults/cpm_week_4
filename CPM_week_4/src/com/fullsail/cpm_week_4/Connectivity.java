/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class Connectivity {
	public boolean isConnected(Context context) {
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    
	    Toast toast = Toast.makeText(context, "No network connection", Toast.LENGTH_SHORT);
	    toast.show();
	    
	    return false;
	}
}
