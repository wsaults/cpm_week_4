/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

public class Celebrity {
	private long id;
	private String firstname;
	private String lastname;
	private int age;
	private String birthdate;
	private String birth_place;
	private float height_in_meters;
	private String celeb_id;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getBirth_place() {
		return birth_place;
	}
	public void setBirth_place(String birth_place) {
		this.birth_place = birth_place;
	}
	public float getHeight_in_meters() {
		return height_in_meters;
	}
	public void setHeight_in_meters(float height_in_meters) {
		this.height_in_meters = height_in_meters;
	}
	public String getCeleb_id() {
		return celeb_id;
	}
	public void setCeleb_id(String celeb_id) {
		this.celeb_id = celeb_id;
	}
}
