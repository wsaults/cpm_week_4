/*
 * project 	CPM_week_4
 * 
 * package 	com.fullsail.cpm_week_4
 * 
 * @author 	William Saults
 * 
 * date 	Nov 21, 2013
 */
package com.fullsail.cpm_week_4;

import java.util.List;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class Sync {
	
	private static Sync instance = null;
	private static Connectivity connection;
	public static String kCelebrityName = "Celebrities";
	private static Boolean isDownloading;
	
	protected Sync() {
		// Exists only to defeat instantiation.
	}
	public static Sync sharedInstance() {
		if(instance == null) {
			instance = new Sync();
			connection = new Connectivity();
			isDownloading = false;
		}
		return instance;
	}

	public void createCelebrity(final Celebrity celebrity) {
		if (celebrity != null && connection.isConnected(MainActivity._context)) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery(kCelebrityName);
	    	query.whereEqualTo("celeb_id", celebrity.getCeleb_id());
	    	query.findInBackground(new FindCallback<ParseObject>() {
			    public void done(List<ParseObject> celebList, ParseException e) {
			        if (e == null) {
			        	// The find succeeded.
			        	// If the record exits in the remote db then dont do anything.
			        	if (celebList.size() == 0) {
			        		ParseObject parseObject = new ParseObject(kCelebrityName);
			        		Log.d("updateCelebrity", "Update obj " + parseObject.getObjectId());
			        		parseObject.put("firstname", celebrity.getFirstname());
			        		parseObject.put("lastname", celebrity.getLastname());
			        		parseObject.put("age", celebrity.getAge());
			        		parseObject.put("birthdate", celebrity.getBirthdate());
			        		parseObject.put("birth_place", celebrity.getBirth_place());
			        		parseObject.put("height_in_meters", celebrity.getHeight_in_meters());
			        		parseObject.put("celeb_id", celebrity.getCeleb_id());
			        		parseObject.saveInBackground();
						}
			        } else {
			        	// Log details of the failure
			            Log.d("downloadCelebrities", "Error: " + e.getMessage());
			        }
			    }
			});
		}
	}
	
	public void updateCelebrity(final Celebrity celebrity) {
		if (celebrity != null && connection.isConnected(MainActivity._context)) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery(kCelebrityName);
	    	query.whereEqualTo("celeb_id", celebrity.getCeleb_id());
	    	query.findInBackground(new FindCallback<ParseObject>() {
			    public void done(List<ParseObject> celebList, ParseException e) {
			        if (e == null) {
			        	// The find succeeded.
			            for (ParseObject parseObject : celebList) {
			            	Log.d("updateCelebrity", "Update obj " + parseObject.getObjectId());
			            	parseObject.put("firstname", celebrity.getFirstname());
			            	parseObject.put("lastname", celebrity.getLastname());
			            	parseObject.put("age", celebrity.getAge());
			            	parseObject.put("birthdate", celebrity.getBirthdate());
			            	parseObject.put("birth_place", celebrity.getBirth_place());
			            	parseObject.put("height_in_meters", celebrity.getHeight_in_meters());
			            	parseObject.put("celeb_id", celebrity.getCeleb_id());
			            	parseObject.saveInBackground();
						}
			        } else {
			        	// Log details of the failure
			            Log.d("downloadCelebrities", "Error: " + e.getMessage());
			        }
			    }
			});
		}
	}

	public void deleteCelebrity(Celebrity celebrity) {
		if (!connection.isConnected(MainActivity._context)) return;
		// Create a local version of the celebrity.
	    Celebrity celeb = celebrity;
	    if (celeb != null) {
	    	ParseQuery<ParseObject> query = ParseQuery.getQuery(kCelebrityName);
	    	query.whereEqualTo("celeb_id", celeb.getCeleb_id());
	    	query.findInBackground(new FindCallback<ParseObject>() {
			    public void done(List<ParseObject> celebList, ParseException e) {
			        if (e == null) {
			        	// The find succeeded.
			            for (ParseObject parseObject : celebList) {
			            	Log.d("deleteCelebrity", "Delete obj " + parseObject.getObjectId());
			            	parseObject.deleteInBackground();
						}
			        } else {
			        	// Log details of the failure
			            Log.d("downloadCelebrities", "Error: " + e.getMessage());
			        }
			    }
			});
	    }
	}

	public void downloadCelebrities() {
		if (!connection.isConnected(MainActivity._context) || isDownloading) return;
		
		isDownloading = true;
		ParseQuery<ParseObject> query = ParseQuery.getQuery(kCelebrityName);
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> celebList, ParseException e) {
		        if (e == null) {
		        	// The find succeeded.
		            Log.d("downloadCelebrities", "Retrieved " + celebList.size() + " celebs");
		            if (celebList.size() > 0) {
		            	// Update the sqlite db
		            	SQLManager.sharedInstance().updateLocalCelebrities(celebList);
		            }
		        } else {
		        	// Log details of the failure
		            Log.d("downloadCelebrities", "Error: " + e.getMessage());
		        }
		        
		        isDownloading = false;
		    }
		});
	}
}
